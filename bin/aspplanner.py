#!/usr/bin/python

import sys, os
sys.path[0] = os.path.join(sys.path[0], '..', 'src')

from cStringIO import StringIO

import term
from utils import gdl_rules_from_file
import dependency
from dependency import functor

def main(filename):
   rules = gdl_rules_from_file(filename) #NB. do not make set --> need roles in order
   positive = set(dependency.run_positive_program(rules))
   actions = set(filter(lambda x : functor(x) == ('does', 2), positive))
   fluents = set(filter(lambda x : functor(x) == ('true', 1), positive))
   less_positive = positive.difference(actions).difference(fluents)
   rigids = dependency.get_rigids(rules)
   actions = map(lambda x : term.term('action', *x.args()), actions)

   rigid_rules = filter(lambda x : functor(x.head()) in rigids, rules)

   inits = filter(lambda x : functor(x.head()) == ('init', 1), rules)
   nexts = filter(lambda x : functor(x.head()) == ('next', 1), rules)
   legals = filter(lambda x : functor(x.head()) == ('legal', 2), rules)
   terminals = filter(lambda x : functor(x.head()) == ('terminal', 0), rules)
   goals = filter(lambda x : functor(x.head()) == ('goal', 2), rules)
   key = set().union(inits, nexts, legals, terminals, goals)

   derived = set(rules).difference(key).difference(rigid_rules)

   print_clingo(key, derived, rigid_rules, rigids, actions)


def print_clingo(key, derived, rigid_rules, rigids, actions):
   s = StringIO()
   for group in (key, derived, rigid_rules):
      s.write('\n\n%%%%%%%%%%%%%%%%\n\n\n')
      for r in group:
         write_rule(s, r, rigids)
   s.write('\n\n%%%%%%% PLANNER %%%%%%%\n\n\n')

   write_clingo_planner(s, actions)
   print s.getvalue()


def write_clingo_planner(s, actions):
   s.write('#const max_time = 20.\n')


   s.write('time(0..max_time).\n')
   s.write('1 { does(R,M,T) : action(R,M) } 1 :- role(R), not terminated(T), time(T).\n')
   s.write('terminated(T) :- terminal(T), time(T).\n')
   s.write('terminated(T+1) :- terminated(T), time(T).\n')
   s.write(':- does(R,M,T), not legal(R,M,T), role(R), time(T).\n')
   s.write(':- 0 { terminated(T) : time(T) } 0.\n')
   s.write(':- terminated(T), not terminated(T-1), role(R), not goal(R, 100, T), time(T).\n')

   s.write('\n\n%%%%%%%% ACTIONS %%%%%%%%\n\n\n')
   for a in actions:
      s.write('%s.\n' % a.as_asp())

#   s.write('1 { final(T) : time(T) : T > 0 } 1.\n')
#   s.write('time(0..max_time).\n')
##   s.write(':- not goal(robot, 100, T), time(T).\n')
#   s.write('win(T) :- terminal(T), goal(R, 100, T), role(R).\n')
#   s.write(':- not win(T), final(T).\n')
#
#   s.write('{ does(R,M,T) } :- legal(R,M,T), role(R), time(T), final(F), T < F.\n')
##   s.write('1 { does(R,M,T) : legal(R,M,T) } 1 :- role(R), time(T), final(F), T <= F.\n')
##   s.write('blerg(0..15).\n')
##   s.write('1 { does(R,M,T) : legal(R,M,T) } 1 :- role(R), blerg(T).\n')

   s.write('#hide. #show does/3.\n')
   #s.write('show final/1. #show legal/3.\n')


def write_rule(s, rule, rigids):
   head = rule.head()
   fun = functor(head)
   time_map = {
      ('init', 1)     : ('0', '0'),
      ('next', 1)     : ('T+1', 'T'),
      ('legal', 2)    : ('T', 'T'),
      ('terminal', 0) : ('T', 'T'),
      ('goal', 2)     : ('T', 'T'),
   }
   time = time_map.get(fun)
   if not time:
      time = ('T', 'T') if fun not in rigids else (None, None)
   head_time, body_time = time

   write_term(s, head, rigids, head_time)
   body = rule.body()
   if body:
      s.write(' :- ')
      comma = ''
      for b in body:
         s.write(comma)
         write_term(s, b, rigids, body_time)
         comma = ', '

   if body_time:
      if body:
         s.write(', time(%s)' % body_time)
      else:
         s.write(' :- time(%s)' % body_time)
   s.write('.\n')


def write_term(s, atom, rigids, time):
   fun = functor(atom)
   if fun == ('not', 1):
      s.write('not ')
      write_term(s, atom.args()[0], rigids, time)
      return

   if fun in (('distinct', 2), ('role', 1)):
      s.write(atom.as_asp())
   elif fun in (('next', 1), ('init', 1), ('true', 1)):
      arg = iter(atom.args()).next()
      s.write('holds(%s, %s)' % (arg.as_asp(), time))
   elif fun in (('does', 2), ('legal', 2), ('terminal', 0), ('goal', 2)):
      args = ', '.join(map(lambda x : x.as_asp(), atom.args()))
      if args:
         args += ', '
      s.write('%s(%s%s)' % (atom.name(), args, time))
   elif fun in rigids:
      s.write('rigid(%s)' % atom.as_asp())
   else:
      s.write('derived(%s, %s)' % (atom.as_asp(), time))


if __name__ == '__main__':
   if len(sys.argv) == 2:
      main(sys.argv[1])
   else:
      print 'usage: %s game.gdl' % os.path.basename(__file__)

