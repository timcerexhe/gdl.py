#!/usr/bin/python

import sys, os, random
sys.path[0] = os.path.join(sys.path[0], '..', 'src')

import converter

if __name__ == '__main__':
   converter.convert_all(sys.argv[1:], 'gdl', 'asp')

