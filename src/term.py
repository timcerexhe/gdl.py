

def parse_term(s):
   from datalog import parser
   terms = parser.parser.quick_parse_gdl_terms(s)
   assert len(terms) == 1
   return terms.pop()


class term:

   lookup_db  = {} #term --> id
   reverse_db = {} #id   --> term

   def __init__(self, name, *args):
      self.id = term.lookup_id(name, *args)
      #assert len(name) > 0
      #print 'new term %d: %s %r' % (self.id, name, args)

   @staticmethod
   def lookup_id(name, *args):
      key = (name, args)
      for a in args:
         assert isinstance(a, term)
      try:
         id = term.lookup_db[key]
      except KeyError:
         id = len(term.lookup_db)
         term.lookup_db[key] = id
         term.reverse_db[id] = key

      return id

   def name(self):
      return term.reverse_db[self.id][0]

   def arity(self):
      return len(self.args())

   def args(self):
      return term.reverse_db[self.id][1]

   def is_variable(self):
      return self.name()[0] == '?'

   def get_variables(self):
      v = set()
      if self.is_variable():
         v.add(self.name())
      for a in self.args():
         v.update(a.get_variables())
      return v

   def rename_variables(self, bad_vars):
      if self.is_variable():
         name = self.name()
         while name in bad_vars:
            name += '_safe_rename' #TODO hack! fix this!
         assert name not in bad_vars
         return term(name)

      else:
         new_args = [ a.rename_variables(bad_vars) for a in self.args() ]
         return term(self.name(), *new_args)

   def is_ground(self):
      if self.is_variable():
         return False
      for a in self.args():
         if not a.is_ground():
            return False
      return True

   def substitute(self, substitution):
      name = self.name()
      h = str(substitution.get(term(name), name))
      a = [ a.substitute(substitution) for a in self.args() ]
      return term(h, *a)

   def __call__(self, i):
      return self.args()[i]

   def __eq__(self, other):
      return (self.id == other.id)

   def __ne__(self, other):
      return (self.id != other.id)

   def __hash__(self):
      return self.id

   def __repr__(self):
      return self.as_gdl()

   def as_gdl(self):
      if len(self.args()) == 0:
         return '%s' % self.name()
      else:
         return '(%s %s)' % (self.name(), ' '.join(map(lambda x : x.as_gdl(), self.args())))

   def as_asp(self):
      def rename(x):
         return x.replace('+', 'asp_plus_magic').replace('-', 'asp_minus_magic').replace('!', 'asp_bang_magic')

      if self.name() == 'not':
         assert self.arity() == 1
         return 'not %s' % self.args()[0].as_asp()
      elif self.name() == 'distinct':
         assert self.arity() == 2
         a,b = self.args()
         return '%s != %s' % (a.as_asp(), b.as_asp())
      elif len(self.args()) == 0:
         if self.is_variable():
            return 'X%s' % rename(self.name()[1:])
         elif self.name() in ('T+1', 'T-1', 't-1') or \
              self.name().startswith('t ==') or \
              self.name().startswith('t !=') or \
              self.name().startswith('t <') or \
              self.name().startswith('t >'): #XXX HACK asp magic
            return '%s' % self.name()
         else:
            return '%s' % rename(self.name())
      else:
         return '%s(%s)' % (rename(self.name()), ','.join(map(lambda x : x.as_asp(), self.args())))

   def as_json(self):
      args = map(lambda a : a.as_json(), self.args())
      return { "type":"term", "name":self.name(), "args":args }

