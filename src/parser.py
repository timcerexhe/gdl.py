import re, subprocess, tempfile
import term, rule, normal_form

GDL_COMMENT_PATTERN  = r';[^\n]*'
GDL_SPACE_PATTERN    = r'\s+'
GDL_SYMBOLS          = r'\(\);'
GDL_ID_PATTERN       = r'([%s]|<=|[^%s;\s]+)' % (GDL_SYMBOLS, GDL_SYMBOLS)

GDL_COMBINED_PATTERN = '|'.join((GDL_COMMENT_PATTERN, GDL_SPACE_PATTERN, GDL_ID_PATTERN))
GDL_PATTERN          = re.compile('(?:%s)' % GDL_COMBINED_PATTERN, re.I)


ASP_COMMENT_PATTERN  = r'\#[^\n]*'
ASP_SPACE_PATTERN    = r'\s+'
ASP_SYMBOLS          = r'\(\)\.,\#{}:\-'
ASP_ID_PATTERN       = r'(:-|[%s]|[^%s\s]+)' % (ASP_SYMBOLS, ASP_SYMBOLS)

ASP_COMBINED_PATTERN = '|'.join((ASP_COMMENT_PATTERN, ASP_SPACE_PATTERN, ASP_ID_PATTERN))
ASP_PATTERN          = re.compile('(?:%s)' % ASP_COMBINED_PATTERN, re.I)


class parser:

   def __show(self, filename, pattern):
      for t in self.tokenise(filename, pattern):
         print t


   def __init__(self, description, flatten=True):
      if description:
         self.rules = self.__tokenise_something(description, GDL_PATTERN, self.parse_gdl_rule)
         if flatten:
            self.rules = normal_form.flatten_rules(self.rules)


   @staticmethod
   def quick_parse_gdl(description):
      p = parser(description)
      return p.rules


   @staticmethod
   def quick_parse_gdl_terms(description):
      rules = parser.quick_parse_gdl(description)
      terms = []
      for r in rules:
         assert r.size() == 0
         terms.append(r.head())
      return terms

   @staticmethod
   def quick_parse_asp(description):
      #print 'QUICK PARSE ASP:', description, '\n'
      p = parser(None)
      rules = p.__tokenise_something(description, ASP_PATTERN, p.parse_asp_rule)
      #print rules
      return rules

   @staticmethod
   def quick_parse_gdl_term(string):
      terms = parser.quick_parse_gdl_terms(string)
      assert len(terms) == 1
      return terms.pop()

   def __tokenise_something(self, description, pattern, rule_parser):
      self.next = self.tokenise(description, pattern).next
      self.token = self.next()
      rules = []
      while self.token != None:
         rules.append(rule_parser())
      return rules


   def parse_asp_rule(self):
      head = self.parse_asp_term()
      if head.name() == '{':
         head = self.parse_asp_term()
         assert self.token == '}'
         self.token = self.next()

      body = []

      if self.token != ':-' and self.token != '.':
         print 'about to be sad:', self.token
      assert self.token == ':-' or self.token == '.'

      exit = (self.token == '.')
      self.token = self.next()
      while not exit:
         body.append(self.parse_asp_term())
         if not self.token in '.,':
            raise Exception('expected . or , but found %s' % self.token) 
         if self.token == '.':
            exit = True
         self.token = self.next()

      return rule.rule(head, *body)


   def parse_asp_term(self):
      name = self.token
      assert name
      if name[0].isupper():
         if len(name) > 1:
            name = name[1:]
         name = '?%s' % name

      args = []
      self.token = self.next()

      if name == 'not':
         args.append(self.parse_asp_term())
      elif self.token == '(':
         self.token = self.next()
         exit = False
         while not exit:
            args.append(self.parse_asp_term())
            assert self.token in ',)'
            if self.token == ')':
               exit = True
            self.token = self.next()

      t = term.term(name, *args)

      if self.token == '!=':
         self.token = self.next()
         right = self.parse_asp_term()
         t = term.term('distinct', t, right)

      return t


   def parse_gdl_rule(self):
      if self.token != '(':
         return rule.rule(self.parse_gdl_term(self.token))

      self.token = self.next()
      if self.token == '<=': #it's a rule
         self.token = self.next()
         head = self.parse_gdl_term(self.token)
         body = []
         while self.token != ')':
            body.append(self.parse_gdl_term(self.token))
         self.token = self.next()

         return rule.rule(head, *body)
      else:
         return rule.rule(self.parse_gdl_term('('))


   def parse_gdl_term(self, previous):
      head = self.token
      if previous == None or previous != '(':
         self.token = self.next()
         return term.term(head)

      if head == '(':
         head = self.next()

      body = []
      self.token = self.next()
      while self.token != ')':
         body.append(self.parse_gdl_term(self.token))

      self.token = self.next()
      return term.term(head, *body)
      

   def tokenise(self, data, pattern):
      for line in data.split('\n'):
         for m in pattern.findall(line):
            if m != '':
               m = m.replace('asp_plus_magic', '+').replace('asp_minus_magic', '-').replace('asp_equal_magic', '=').replace('asp_bang_magic', '!')
               yield m

      yield None

