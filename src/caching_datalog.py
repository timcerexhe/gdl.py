
def caching_mixin(unwrapped_prove_all):

   #force closure of parent scope into wrapped_prove_all
   #with arrays, because even python sucks sometimes
   cache = { unwrapped_prove_all : {} }

   def wrapped_prove_all(self, assumptions=set()):
      key = hash(frozenset(assumptions))
      if key in cache[unwrapped_prove_all].keys():
         return cache[unwrapped_prove_all][key]
      else:
         result = unwrapped_prove_all(self, assumptions)
         cache[unwrapped_prove_all][key] = result
         return result

   return wrapped_prove_all

