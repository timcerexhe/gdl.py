import sys
from parser import parser

RECOGNISED_FORMS = ('gdl', 'asp')

def convert_all(domains, in_form, out_form):
   if not domains:
      domains.append('-')

   for d in domains:
      if d == '-h':
         usage()

   if len(filter(lambda x : x == '-', domains)) > 1:
      print '\033[31mstdin only allowed once!\033[0m'
      usage()

   for who,form in (('input', in_form), ('output', out_form)):
      if form not in RECOGNISED_FORMS:
         choices = ', '.join(RECOGNISED_FORMS)
         print 'unrecognised %s format "%s", choose from [%s]' % (who, form, choices)
         usage()

   for d in domains:
      if d == '-':
         convert_file(sys.stdin, in_form, out_form)
      else:
         with open(d, 'r') as f:
            convert_file(f, in_form, out_form)

def convert_file(f, in_form, out_form):
   assert out_form in ('gdl', 'asp')
   out_form = 'as_%s' % out_form

   description = f.read()
   method = 'quick_parse_%s' % in_form
   method = getattr(parser, method)
   rules = method(description)
   for r in rules:
      print getattr(r, out_form)()

def usage():
   print 'usage: python %s games...' % os.basename(__file__)
   print '(for stdin, use - or no args)'
   sys.exit(1)

