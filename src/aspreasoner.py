from cStringIO import StringIO
import subprocess

from stratifier import functor
from parser import parser
from rule import rule
from caching_datalog import caching_mixin


import time, sys

GRINGO = 'gringo'


class aspreasoner(object):

   def __init__(self, rules):
      s = StringIO()
      for r in rules:
         s.write(r.as_asp() + '\n')

      self.rules = s.getvalue()


   def prove(self, fun_name, fun_arity, assumptions=set()):
      facts = self.prove_all(assumptions)
      fun = (fun_name, fun_arity)
      return filter(lambda x : functor(x) == fun, facts)



   #@caching_mixin
   def prove_all(self, assumptions=set()):
      rules = StringIO()
      rules.write(self.rules)
      for a in assumptions:
         rules.write(a.as_asp() + '.\n')
      gringo = subprocess.Popen([GRINGO, '-t'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out,err = gringo.communicate(rules.getvalue())

      #print 'GRINGO SAYS:', out

      duration = time.time()
      rules = parser.quick_parse_asp(out)
      for r in rules:
         assert r.size() == 0
      terms = map(lambda x : x.head(), rules)
      duration = time.time() - duration
      #sys.stderr.write(str(duration) + '\n')
      return terms

