import term, rule
from stratifier import functor
import itertools

def disjunctive_clause(clauses):
   if len(clauses) == 1:
      return clauses[0]
   return term.term('or', *clauses)

def conjunctive_clause(clauses):
   if len(clauses) == 1:
      return clauses[0]
   return term.term('and', *clauses)

def clause_contains(clause, form):
   if clause.arity() == 0:
      return False
   elif clause.name() == form:
      return True

   for c in clause.args():
      if clause_contains(c, form):
         return True
   return False


#convert a meta-term (and, or) to negation normal form
#NB. we can ignore distinct here since that is already
#in nnf, we just need to worry about not(and(...)) + not(or(...)) terms
def to_nnf(clause):
   fn = functor(clause)
   name,arity = fn

   #the only form of negation we recognise!
   if fn == ('not', 1):
      inner = clause.args()[0]
      fn = functor(inner)
      name, arity = fn

      #remove double negation and recurse
      if fn == ('not', 1):
         inner = inner.args()[0]
         return to_nnf(inner)

      #apply de morgan's and recurse on meta-terms
      elif name in ('and', 'or'):
         new_name = 'and' if name == 'or' else 'or'
         args = map(lambda x : to_nnf(term.term('not', x)), inner.args())
         return term.term(new_name, *args)

      #it's simply a negative literal, already nnf
      else:
         return clause

   #it's a meta-term, so recurse
   elif name in ('and', 'or'):
      args = map(to_nnf, clause.args())
      return term.term(name, *args)

   #it's just a regular term, already nnf
   else:
      return clause


#convert a meta-term (and, or) to disjunctive normal form
def to_dnf(clause):
   #convert to negation normal form
   clause = to_nnf(clause)
   return to_normal_form(clause, 'or', 'and')

def to_cnf(clause):
   #convert to negation normal form
   clause = to_nnf(clause)
   return to_normal_form(clause, 'and', 'or')

def to_normal_form(clause, outer_form, inner_form):
   #NB. we do the negation-normal form outside so we
   #don't have to repeat it here when we recurse

   #we don't need to standardise variables -- everything
   #implicitly universally quantified, no explicit quantifiers
   #similarly we don't need to skolemize or drop any quantifiers

   #flatten + distribute conjunctions
   clause = flatten(clause)
   name = clause.name()
   if name == inner_form:
      clause = distribute(clause)
      if clause.name() == outer_form: #we distributed, check everything's done
         return to_normal_form(clause, outer_form, inner_form)
      else: #no distribution -- this conjunct *should* contain no disjuncts
         assert not clause_contains(clause, outer_form)
         return clause

   elif name == 'or':
      return term.term('or', *map(to_dnf, clause.args()))

   else:
      return clause


def flatten(clause):
   name = clause.name()
   if name not in ('and', 'or'):
      return clause

   old_args = list(clause.args())
   new_args = []
   while old_args:
      a = old_args.pop()
      if a.name() == name: #to flatten!
         old_args.extend(a.args())
      else:
         new_args.append(a)

   return term.term(name, *new_args)


def flatten_rules(rules):
   flat_rules = []
   for r in rules:
      dnf = to_dnf(conjunctive_clause(r.body()))
      dnf = to_clause_form(dnf, 'or')
      for minterm in dnf:
         flat_rules.append(rule.rule(r.head(), *minterm))
   return flat_rules


def distribute(clause):
   form = clause.name()
   assert form in ('and', 'or')
   other = 'and' if form == 'or' else 'or'

   #if the clause has nothing to distribute over then we're done
   if not clause_contains(clause, other):
      return clause

   product = []
   for a in clause.args():
      if a.name() == other: #distribution target
         product.append(a.args())
      else:
         assert a.name() != form #not flat!
         product.append([ a ])

   clauses = []
   for p in itertools.product(*product):
      if len(p) == 1: #TODO doesn't seem right ... append p.pop()?
         clauses.append(p)
      else:
         clauses.append(term.term(form, *p))

   return term.term(other, *clauses)


def to_clause_form(clause, form):
   def convert(clause):
      if clause.name() in ('and', 'or'):
         return map(convert, clause.args())
      else:
         return clause

   outer = clause.name()
   clause = convert(clause)

   #if no list then the whole "clause thing" is singleton
   if not isinstance(clause, list):
      return [[ clause ]]

   #we have a list ...

   #... but not on the thing we wanted, so also wrap
   if outer != form:
      return [ clause ]

   #... on the thing we wanted, make sure the inner thing is also wrapped
   outer_clause = []
   for c in clause:
      if not isinstance(c, list):
         c = [ c ]
      outer_clause.append(c)

   return outer_clause


