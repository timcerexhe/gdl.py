import graph

def subgraphs (g):
   #HACK: create a undirected copy (unlabelled!), then check for SCCs!
   uni = graph.digraph ()
   for v1 in g.get_vertices ():
      uni.add_vertex (v1)

      for v2 in g.get_vertices ():
         uni.add_vertex (v2)

         if g.is_edge (v1, v2):
            uni.add_edge (v1, v2, None)
            uni.add_edge (v2, v1, None)

   t = tarjan (uni)
   return t.get_SCCs ()


def get_subgraph (g, node):
   the_subgraphs = subgraphs (g)
   for sg in the_subgraphs:
      if node in sg:
         return sg
   raise Exception ('requested node ' + node + ' not in graph!')


class tarjan:

   def __init__(self, g):
      self.SCCs = []
      self.g = g
      self.index = 0
      self.S = []
      self.indices = {}

      vertices = g.get_vertices()

      for v in vertices:
         if not self.indices.has_key(v): #this node's index is undefined
            self.__strong_connect(v)


   def __strong_connect(self, v):
      self.indices[v] = (self.index, self.index) #(index, lowlink)
      self.index += 1
      self.S.append(v)
      for w in self.g.get_connections(v):
         vInd,vLow = self.indices[v]

         if not self.indices.has_key(w): #successor is undefined, so recurse
            self.__strong_connect(w)
            wInd,wLow = self.indices[w]
            self.indices[v] = (vInd, min(vLow, wLow))

         elif w in self.S: #successor already in this SCC
            wInd,wLow = self.indices[w]
            self.indices[v] = (vInd, min(vLow, wInd))

      vInd,vLow = self.indices[v]
      if vInd == vLow: #v is the root of this SCC
         scc = []
         go = True
         while go:
            w = self.S.pop()
            scc.append(w)
            if w == v:
               go = False

         #print 'found SCC:', scc
         self.SCCs.append(scc)


   def get_SCCs(self):
      return self.SCCs


   #we distinguish between strongly connected components, self-loops, and random
   #nodes that aren't strongly connected (or are disjoint, called "islands")
   def get_all_cycles(self):
      sccs = []
      selfloops = []
      islands = []

      for s in self.SCCs:
         if len(s) == 1:
            v = s[0]
            if self.g.is_edge(v,v):
               selfloops.append(s)
            else:
               islands.append(s)
         else:
            sccs.append(s)

      return (sccs, selfloops, islands)



class astar:

   def __init__(self, g):
      self.g = g

   def has_path(self, a, b):
      assert NotImplemented


class dijkstra:

   def __init__(self, g, v):
      self.graph = g
      self.connections = set([ v ])
      self.parent = { v:None }
      self.__find_connections(v)

   def __find_connections(self, root):
      conns = self.graph.get_connections(root)
      for c in conns:
         if c not in self.connections:
            self.connections.add(c)
            self.parent[c] = root
            self.__find_connections(c)

   def has_path(self, v):
      return v in self.connections

   def get_path(self, v):
      assert v in self.connections
      path = [ v ]
      p = self.parent[v]
      while p != None:
         path.append(p)
         p = self.parent[p]

      path.reverse()
      assert len(path) < 2 or path[0] != v #TODO do this until we're sure path.reverse() works ...
      return path


