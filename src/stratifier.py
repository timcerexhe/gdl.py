from graph import digraph
import graph_algos

def functor(t):
   return (t.name(), t.arity())

def term_to_fs(t):
   if t.name() == 'not':
      assert t.arity() == 1
      t = t.args()[0]
   assert t.name() != 'not' #don't be a jerk; no double negations allowed!
   return (t.name(), t.arity())


def check_stratified(graph):
   scc,selfloops,islands = graph_algos.tarjan(graph).get_all_cycles()
   #print '%d STRONGLY CONNECTED COMPONENTS' % len(scc), scc
   #print '%d SELF LOOPS' % len(selfloops), selfloops
   #print 'AND %d ISLANDS' % len(islands)

   for s in scc:
      for v in s:
         #TODO fix this hack: check every pairwise edge for a potential negative edge
         #it's not too bad though: SCCs are very rare and should be small
         #and sparsely connected when they do occur ...
         for w in s:
            labels = graph.get_labels(v,w)
            if not labels:
               continue

            for l in labels:
               if l == False:
                  raise ValueError('GDL is not stratified: cycle with negative edge (%s, %s)' % (v, w))

   for s in selfloops:
      assert len(s) == 1
      v = s[0]
      assert graph.is_edge(v, v)
      labels = graph.get_labels(v,v)
      if not labels:
         continue

      for l in labels:
         if l == False:
            raise ValueError('GDL is not stratified: negative self loop on %s' % v)

   return scc,selfloops,islands



def generate_dependency_graph(rules):
   g = digraph()

   #nodes are supposed to be IDB predicates = terms that can be generated (eg. next)
   #(as opposed to EDB predicates which have to be provided, eg. true, does -- no "rules" for these)
   nodes = set([ term_to_fs(r.head()) for r in rules ])
   for n in nodes:
      g.add_vertex(n)

   for r in rules:
      f = term_to_fs(r.head())
      for a in r.body():
         t = term_to_fs(a)
         l = False if a.name() == 'not' else True
         if t in nodes:
            g.add_edge(f, t, l)
   return g



def get_strata(rules, graph):
   stratum = {}
   highest = 0
   changed = True
   count = len(rules)

   def get(fn):
      if not stratum.has_key(fn):
         stratum[fn] = 0
      return stratum[fn]

   while changed and highest <= count:
      changed = False

      for r in rules:
         hp = functor(r.head())
         if r.size() == 0:
            get(hp) #hack: set to zero if not already exists!
         else:
            for b in r.body():
               atom = b
               fun = functor(atom)
               if fun == ('distinct', 2):
                  continue

               pos = True
               while fun == ('not', 1):
                  atom = atom.args()[0]
                  fun = functor(atom)
                  pos = not pos

               hp_strat = get(hp)
               b_strat = get(fun)
               if pos:
                  greater = max(hp_strat, b_strat)
                  if hp_strat < greater:
                     stratum[hp] = greater
                     changed = True
                  highest = max(highest, greater)

               elif b_strat >= hp_strat:
                  stratum[hp] = b_strat + 1
                  highest = max(highest, b_strat + 1)
                  changed = True

   if highest > count:
      raise InvalidArgumentException('rules are not stratified')

   strata = [ set() for i in xrange(highest+1) ]
   for f,s in stratum.items():
      strata[s].add(f)
   return strata


def stratify_rules(strata, rules):
   #generate a quick lookup of function symbol (name,arity) --> stratum level
   stratum_lookup = {}
   for s,level in zip(strata, xrange(len(strata))):
      for fs in s:
         stratum_lookup[fs] = level

   #generate an empty list of rules for each stratum
   strata_rules = [ [] for s in strata ]

   #now go through all the rules, lookup their level, and categorise the rule
   for r in rules:
      fs = term_to_fs(r.head())
      level = stratum_lookup[fs]
      strata_rules[level].append(r)

   #sanity check and we're done :)
   assert len(strata) == len(strata_rules)
   return strata_rules


# not perfect, but should work for GDL provided rules are conformant
# (safe, all variables bound by facts and other body terms, etc.)
# and facts are GROUND (important!)
def unify(a, b):
   if a.is_variable():
      return { a:b }
   if b.is_variable():
      return { b:a }
   if a.name() != b.name() or a.arity() != b.arity():
      return None

   size = a.arity()
   uni = {}
   for i in xrange(size):
      ai = a(i)
      bi = b(i)
      sub = unify(ai, bi)
      if sub == None:
         return None
      for k,v in sub.items():
         if uni.get(k,v) != v: #print 'ERROR: %s [%s] != %s :s' % (uni, k, v)
            return None

         assert uni.get(k, v) == v #TODO this is probably not an error; should just return None?
         uni[k] = v

   return uni




