import itertools

GDL_KEYWORDS = ( ('true',1), ('next',1), ('legal',2), ('does',2), ('distinct',2), ('terminal', 0), ('goal',2), ('init',1), ('role',1), ('sees',2), )


def read_gdl_from_filename(filename):
   if filename == '-':
      import sys
      raw = sys.stdin.read()
   else:
      with open(filename, 'r') as f:
         raw = f.readlines()
   return clean_gdl(raw)


def gdl_rules_from_file(filename):
   gdl = read_gdl_from_filename(filename)
   import parser
   p = parser.parser(gdl)
   return p.rules


#the c++ reasoner is VERY particular about what "gdl looks like" ... BAH!
#seems to be: all on one line (and hence no comments!)
def clean_gdl(raw):
   rules = []
   for r in raw:
      r = r.strip()
      i = r.find(';')
      if i >= 0:
         r = r[:i]
      rules.append(r)
   return ' '.join(rules)


def joint_moves(state, gdl, roles=None):
   if not roles:
      roles = gdl.get_roles()

   #print 'joint moves'
   #print roles
   #for r in roles:
   #   gdl.
   legals = [ gdl.get_legals(r, state) for r in roles ]
   return itertools.product(*legals)

