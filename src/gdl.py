import random
import parser, term
#reasoner imports: seminaive, forward_datalog, aspreasoner
import aspreasoner
from stratifier import functor
from utils import joint_moves

DATALOG_ENGINE = aspreasoner.aspreasoner
#DATALOG_ENGINE = seminaive.seminaive
#DATALOG_ENGINE = forward_datalog.forward_datalog

if False:
   from reasoners.Seminaive.seminaive import seminaive
   DATALOG_ENGINE = seminaive


class termlist:
   def __init__(self, terms):
      self.terms = terms

   def facts(self):
      return self.terms



class gdl:

   def __init__(self, gdl_description, engine=DATALOG_ENGINE):
      if isinstance(gdl_description, str):
         parse = parser.parser(gdl_description)
         self.prover = engine(parse.rules)
         self.rules = parse.rules
      else:
         self.prover = engine(gdl_description)
         self.rules = gdl_description

      self.__roles = []
      for r in self.rules:
         if r.size() == 0 and functor(r.head()) == ('role', 1):
            self.__roles.append(r.head().args()[0])

      self.identifiers = set()
      for r in self.rules:
         head = r.head()
         if functor(head) in (('next',1), ('init',1)):
            self.identifiers.add(head.args()[0].name())
         elif functor(head) == ('legal',2):
            self.identifiers.add(head.args()[1].name())


   def __repr__(self):
      return '<GDL: %s>' % '/'.join(self.identifiers)


   def get_initial_state(self):
      facts = self.prover.prove('init', 1)
      #facts = self.__filter(facts, 'init')
      return termlist(self.__strip(facts, 'init', 0))

   def get_roles(self):
      #facts = self.prover.prove('role', 1)
      #found = self.__filter(facts, 'role')
      #roles = []
      #for r in facts:
      #   assert r.arity() == 1
      #   roles.append(r.args()[0])
      #assert set(self.__roles) == roles #sanity check
      return self.__roles #because this is ordered

   def all_legals(self, state):
      facts = self.prover.prove('legal', 2, self.__state_facts(state))
      #facts = self.__filter(facts, 'legal')
      facts = self.__replace(facts, 'legal', 'does')

      legals = {}
      for f in facts:
         args = f.args()
         assert len(args) == 2
         role, move = args

         if not legals.has_key(role):
            legals[role] = []
         legals[role].append(f)

      for r,l in legals.items():
         legals[r] = list(self.__strip(legals[r], 'does', 1)) #want to index

      return legals

   def get_legals(self, role, state):
      moves = self.all_legals(state)
      #print 'MOVES:', moves, 'in', state.facts()
      return moves[role]

   def update(self, moves, state):
      m = self.__move_facts(moves)
      s = self.__state_facts(state).union(m)
      nexts = self.prover.prove('next', 1, s)
      return termlist(self.__strip(nexts, 'next', 0))

   def update_with_sees(self, state, moves):
      state = self.__state_facts(state)
      moves = self.__move_facts(moves)
      nexts = update(self, state)
      s = state.union(moves)
      sees = termlist(self.prover.prove('sees', 2, s))
      return (nexts, sees)

   def terminal(self, state):
      facts = self.prover.prove('terminal', 0, self.__state_facts(state))
      return (term.term('terminal') in facts)

   def goals(self, state):
      goals = self.prover.prove('goal', 2, self.__state_facts(state))
      #goals = self.__filter(facts, 'goal')
      gmap = {}
      for g in goals:
         args = g.args()
         assert len(args) == 2
         name, value = args
         assert name not in gmap.keys()
         assert value.arity() == 0
         gmap[name] = int(value.name())
      return gmap

   def goal(self, role, state):
      gmap = self.goals(state)
      try:
         return gmap[role]
      except:
         raise Exception('state with undefined goal value for %s\nSTATE: %s' % (role, state))

   def holds(self, state, conjunction):
      truth = set(self.prover.prove_all(state.facts()))
      conjunction = set(conjunction)
      neg = set(filter(lambda x : functor(x) == ('not', 1), conjunction))
      pos = conjunction.difference(neg)
      return pos.issubset(truth) and not neg.intersection(truth)

   def term(self, s):
      return parser.parser.quick_parse_gdl_term(s)


   def __state_facts(self, state):
      return set([ term.term('true', f) for f in state.facts() ])

   def __move_facts(self, moves):
      return set([ term.term('does', r, m) for r,m in zip(self.__roles, moves) ])

   def __filter(self, facts, name): #MUST PRESERVE ORDER!
      return filter(lambda f : f.name() == name, facts)

   def __replace(self, facts, name, new_name):
      clean = []
      for f in facts:
         if f.name() == name:
            clean.append(term.term(new_name, *f.args()))
         else:
            clean.append(f)
      return set(clean)

   def __strip(self, facts, name, argindex):
      clean = []
      for f in facts:
         assert f.name() == name
         clean.append(f.args()[argindex])
      return set(clean)

   def generate_state(self, cutoff):
      roles = self.get_roles()
      state = self.get_initial_state()
      depth = 0
      while not self.terminal(state) and depth < cutoff:
         jm = random.choice(list(joint_moves(state, self, roles)))
         state = self.update(jm, state)
         depth += 1
      return state


