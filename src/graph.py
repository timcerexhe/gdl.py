from cStringIO import StringIO

class digraph:

   def __init__(self):
      self.conns = {}
      self.labels = {}
      self.in_cache = {}

   def size(self):
      return len(self.conns)

   def add_vertex(self, name):
      n = self.conns.get(name)
      if n is None:
         if not self.conns.has_key(name):
            self.conns[name] = set()
            self.__changed()

   def add_edge(self, a, b, label):
      #if isinstance(a,tuple) and isinstance(b,tuple):
      #   if a == ('index',1,0) and b == ('location',3,None):
      #   if a[0:2] == ('legal',2) and b[0:2] == ('legal',2):
      #      print 'HERE IT IS:', a, b, label
      #      assert a[2] == 1
      #      assert a[2] != 0
      #      assert False
      #      assert label != None

      assert self.is_vertex(a) and self.is_vertex(b)
      self.conns[a].add(b)
      l = self.get_labels(a, b)
      if l is None:
         self.labels[(a,b)] = set()

      if label not in self.labels[(a,b)]:
         self.labels[(a,b)].add(label)
         self.__changed()

   def get_vertices(self):
      return self.conns.keys()

   def get_connections(self, name):
      if not self.conns.has_key(name):
         print 'eh? could not find "%s" (%r) in graph?' % (name, name)
         assert False
      return self.conns[name]

   def get_inputs(self, name):
      if not self.in_cache.has_key(name):
         self.in_cache[name] = filter(lambda x : self.is_edge(x,name), self.get_vertices())
      return self.in_cache[name]

   def is_vertex(self, name):
      return self.conns.has_key(name)

   def is_edge(self, a, b):
      return b in self.conns.get(a,set())

   def get_labels(self, a, b):
      return self.labels.get((a,b))

   def remove_all_edges(self, a, b):
      self.in_cache = {}
      self.conns[a].discard(b)
      if self.labels.has_key((a,b)):
         del self.labels[(a,b)]
         self.__changed()

   def remove_label(self, a, b, l):
      self.in_cache = {}
      if self.labels.has_key((a,b)):
         self.labels[(a,b)].discard(l)

         if len(self.labels[(a,b)]) == 0:
            self.remove_all_edges(a,b)

   def remove_vertex(self, node):
      self.in_cache = {}

      #from datalog import term
      #if (isinstance(node, term.term) and node.name() == 'step') or \
      #   (len(node) == 2 and node[0] == 'step'):
      #   print 'REMOVING:', node
      #   #assert False

      for v in self.get_vertices():
         if self.is_edge(v,node):
            self.remove_all_edges(v,node)
         if self.is_edge(node,v):
            self.remove_all_edges(node,v)
      if node in self.conns:
         del self.conns[node]
         self.__changed()

   def dot_node_properties(self, name, vertex_writer, other_props=None):
      extra = other_props(name) if other_props else None

      if extra:
         extra = '%s,' % extra
      else:
         extra = 'shape=square,style=filled,fillcolor=lightgrey,'

      return '%slabel="%s"' % (extra, vertex_writer(name))

   def to_dot(self, name='digraph', vertex_writer=lambda x : str(x), other_props=None):
      s = StringIO()
      s.write('digraph "%s" {\n' % name)
      for node,connections in self.conns.items():
         s.write('   "%s"[%s];\n' % (vertex_writer(node), self.dot_node_properties(node, vertex_writer, other_props)))
         for c in connections:
            labels = self.get_labels(node, c)
            if len(labels) == 0:
               labels = set([ None ]) #we know there's a connection, so make sure we print SOMETHING

            for l in labels:
               s.write('   "%s"->"%s"' % (vertex_writer(node), vertex_writer(c)))
               if l != None:
                  s.write('[label="%s"]' % str(l))
               s.write(';\n')

      s.write('}\n')
      return s.getvalue()

   def to_json_dict(self, name='digraph'): #TODO? vertex_writer=lambda x : str(x), other_props=None):
      j = { 'name':name, 'vertices':[], 'edges':[] }
      for node,connections in self.conns.items():
         j['vertices'].append({ 'name':node })
         for c in connections:
            labels = self.get_labels(node, c)
            if len(labels) == 0:
               labels = set([ None ])

            for l in labels:
               edge = { 'from':node, 'to':c }
               if l:
                  edge['label'] = l
               j['edges'].append(edge)

      return j

   def __repr__(self):
      return self.to_dot()

   #a lazy shallow copy :(
   def copy(self):
      g = digraph()
      for v1 in self.conns.keys():
         g.add_vertex(v1)
         for v2 in self.conns.keys():
            g.add_vertex(v2)
            if self.is_edge(v1,v2):
               labels = self.get_labels(v1,v2)
               for l in labels:
                  g.add_edge(v1,v2,l)
      return g


   def __changed (self):
      #self.video ()
      pass


   def video (self):
      if not hasattr(self, 'video_frame'):
         self.video_frame = 0

      import os
      path = '/tmp/vargraph_videos'
      if not os.path.exists(path):
         os.mkdir(path)

      with open(os.path.join(path, 'frame%05d.dot' % self.video_frame), 'w') as f:
         f.write(self.to_dot())

      self.video_frame += 1

