import subprocess, sys
from cStringIO import StringIO

import term, rule
from parser import parser
import graph
import graph_algos

def functor(t):
   return (t.name(), t.arity())

def get_functor_name(t):
   fun = functor(t)
   positive = True
   while fun == ('not', 1):
      assert t.arity() != 0
      t = t.args()[0]
      fun = functor(t)
      positive = not positive
   return (fun, positive)

def generate_dependency_graph(rules):
   g = graph.digraph()
   for r in rules:
      h,pos = get_functor_name(r.head())
      assert pos #gdl can't have negative terms in the head of a rule
      g.add_vertex(h)
      for b in r.body():
         depend,pos = get_functor_name(b)
         g.add_vertex(depend)
         g.add_edge(h, depend, pos)
   return g

def get_rigids(rules):
   depgraph = generate_dependency_graph(rules)
   with open('/tmp/depgraph.dot', 'w') as f:
      f.write(depgraph.to_dot())
   import subprocess
   subprocess.check_call(['dot', '-Tpdf', '-o', '/tmp/depgraph.pdf', '/tmp/depgraph.dot'])
   return get_rigids_from_mutable_depgraph(depgraph)

def get_rigids_from_depgraph(depgraph):
   return get_rigids_from_mutable_depgraph(depgraph.copy())

def get_rigids_from_mutable_depgraph(depgraph):
   true = ('true', 1)
   depgraph.add_vertex(true)

   mandatory = (('next', 1), ('legal', 2), ('does', 2), ('goal', 2), ('init', 1), ('terminal', 0))
   for m in mandatory:
      depgraph.add_vertex(m)
      depgraph.add_edge(m, true, True)

   rigids = set(depgraph.get_vertices())
   rigids.discard(true)
   dynamic = set([true])
   changed = True
   while changed:
      changed = False
      local_changes = set()
      for v in dynamic:
         inputs = set(depgraph.get_inputs(v))
         new = inputs.difference(dynamic)
         if new:
            changed = True
            local_changes.update(new)
      rigids = rigids.difference(local_changes)
      dynamic.update(local_changes)

   #print 'RIGIDS:', rigids
   return rigids

def run_positive_program(rules, grounder='gringo'):
   program = StringIO()
   for r in rules:
      head = r.head()
      head_fn = functor(head)
      if head_fn in (('init', 1), ('next', 1)):
         head = term.term('true', *head.args())
      elif head_fn == ('legal', 2):
         head = term.term('does', *head.args())
      elif not head.name().strip(): #XXX this makes Abdallah cry
         continue

      new_body = []
      for b in r.body():
         fn = functor(b)
         if fn not in (('not', 1), ('distinct', 2)):
            new_body.append(b)

      if False and head_fn == ('legal', 2):
         program.write('{ %s }' % head.as_asp())
      else:
         program.write(head.as_asp())

      if new_body:
         program.write(' :- %s' % ', '.join([ b.as_asp() for b in new_body ]))
      program.write('.\n')

   with open('/tmp/positive.asp', 'w') as f:
      f.write(program.getvalue())

   rules = ground(program.getvalue(), grounder)
   terms = []
   for r in rules:
      assert r.size() == 0
      terms.append(r.head())
   return terms


def ground(program_string, grounder='gringo'):
   cmd = [ grounder, '-t' ]
   #TODO flags?
   proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = proc.communicate(program_string)
   if err:
      sys.stderr.write('\033[31mERRORS: ' + err + '\033[0m\n')
   #print 'POSITIVE:', out

   #with open('/tmp/bah.gdl', 'w') as f:
   #   for r in rules:
   #      f.write(r.as_gdl() + '\n')
   #print 'original gdl written to /tmp/bah.gdl'

   #with open('/tmp/positive.asp', 'w') as f:
   #   f.write(program.getvalue())
   #print 'positive program written to /tmp/positive.asp'

   with open('/tmp/bah', 'w') as f:
      f.write(out)
   rules = parser.quick_parse_asp(out)
   return rules


def ground_rules(rules, fluents, actions):
   return ground(grounder_program_string(rules, fluents, actions))
   

def grounder_program_string(rules, fluents, actions):
   s = StringIO()
   for f in fluents:
      s.write('{ %s }.\n' % f.as_asp())

   for a in actions:
      s.write('{ %s }.\n' % a.as_asp())

   s.write('\n')
   for r in rules:
      head = r.head()
      fun = functor(head)

      s.write(r.as_asp())
      s.write('\n')
   return s.getvalue()


def get_action_domains(positive_program, prefix=None):
   return get_functor_domains(positive_program, ('does', 2), prefix)

def get_fluent_domains(positive_program, prefix=None):
   return get_functor_domains(positive_program, ('true', 1), prefix)

def get_functor_domains(positive_program, fun, prefix=None):
   domains = filter(lambda x : functor(x) == fun, positive_program)
   if prefix:
      domains = map(lambda x : term.term(prefix, *x.args()), domains)

   return domains


