import term

def safe_reorder_terms(terms):
   return tuple(sorted(terms, cmp=safe_rule_body_comparer))

def safe_rule_body_comparer(a,b):
   na = a.name() in ('not', 'distinct')
   nb = b.name() in ('not', 'distinct')
   if na and not nb: #a is negative, b is not, so they're around the wrong way = 1
      return 1
   elif nb and not na: #reverse of above
      return -1
   else: #otherwise they're both positive or both negative, so just compare on id
      return cmp(a.id, b.id)
      #return cmp(str(a), str(b)) #TODO do we want/need stronger ordering like this?


class rule:

   lookup_db  = {} #rule --> id
   reverse_db = {} #id   --> rule

   def __init__(self, head, *args):
      self.id = rule.lookup_id(head, *args)
      #print 'new rule %d: %s %r' % (self.id, head, args)

   @staticmethod
   def lookup_id(head, *args):
      assert isinstance(head, term.term)
      for a in args:
         assert isinstance(a, term.term)

      key = (head, safe_reorder_terms(args))

      try:
         id = rule.lookup_db[key]
      except KeyError:
         id = len(rule.lookup_db)
         rule.lookup_db[key] = id
         rule.reverse_db[id] = key
      return id

   def head(self):
      return rule.reverse_db[self.id][0]

   def body(self):
      return rule.reverse_db[self.id][1]

   def size(self):
      return len(self.body())

   def top(self):
      return self(0)

   def pop(self):
      return rule(self.head(), *self.body()[1:])

   def rename_variables (self, bad_vars):
      h = self.head ().rename_variables (bad_vars)
      b = [ b.rename_variables (bad_vars) for b in self.body () ]
      return rule (h, *b)

   def get_variables(self):
      v = self.head().get_variables()
      for b in self.body():
         v.update(b.get_variables())
      return v

   def is_ground(self):
      if not self.head().is_ground():
         return False
      for b in self.body():
         if not b.is_ground():
            return False
      return True

   def substitute(self, substitution):
      h = self.head().substitute(substitution)
      b = [ b.substitute(substitution) for b in self.body() ]
      return rule(h, *b)

   def __call__(self, i):
      return self.body()[i]

   def __eq__(self, other):
      return (self.id == other.id)

   def __ne__(self, other):
      return (self.id != other.id)

   def __hash__(self):
      return self.id

   def __repr__(self):
      return self.as_gdl()

   def as_gdl(self):
      if self.size():
         return '(<= %s %s)' % (self.head().as_gdl(), ' '.join(map(lambda x : x.as_gdl(), self.body())))
      else:
         return '%s' % self.head().as_gdl()

   def as_asp(self):
      if self.size():
         return '%s :- %s.' % (self.head().as_asp(), ','.join(map(lambda x : x.as_asp(), self.body())))
      else:
         return '%s.' % self.head().as_asp()

   def as_json(self):
      head = self.head().as_json()
      body = map(lambda b : b.as_json(), self.body())
      return { "type":"rule", "head":head, "body":body }

